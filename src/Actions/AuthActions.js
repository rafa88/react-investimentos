import firebase from "../firebase";

export const createUser = (values, callback) => async (dispatch) => {
  const { email, password } = values;
  dispatch({ type: "FETCH_SIGNUP" });

  try {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((data) => {
        dispatch({
          type: "SIGNUP_SUCCESS",
          value: { user: data.user, idToken: data.user.xa },
        });
        sessionStorage.setItem("reactInvestimentoToken", data.user.xa);

        callback()
        firebase.auth().onAuthStateChanged(function (user) {
          user.sendEmailVerification();
        });
      })
      .catch((error) => {
        dispatch({ type: "SIGNUP_ERROR", value: { signUp: error.code } });
      });
  } catch (error) {
    dispatch({ type: "SIGNUP_ERROR", value: { signUp: error.code } });
  }
};

export const signIn = (values, callback) => async (dispatch) => {
  const { email, password } = values;
  dispatch({ type: "FETCH_SIGNIN" });

  try {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((data) => {
        if (data.user.emailVerified) {
          firebase
            .auth()
            .currentUser.getIdToken(true)
            .then(function (idToken) {
              dispatch({ type: "SIGNIN_SUCCESS", value: {user: data.user, idToken: idToken} });
              sessionStorage.setItem('reactInvestimentoToken', idToken)
              callback();
            })
        } else {
          dispatch({ type: "SIGNIN_ERROR", value: {signIn: 'Email/Senha incorreto'} });
        }
      })
      .catch((error) => {
        dispatch({ type: "SIGNIN_ERROR", value: { signIn: error.code } });
      });
  } catch (error) {
    dispatch({ type: "SIGNIN_ERROR", value: { signIn: error } });
  }
};

export const signOut = () => async (dispatch, getState) => {
  dispatch({ type: "SIGNOUT" });

  sessionStorage.removeItem('reactInvestimentoToken')
  firebase.auth().signOut();
};
