import * as d3 from 'd3';
import Chart from '../Chart';
import { mapIncome } from '../../../Enum';
import { maskPrice } from '../../Helpers/Mask';

import "./Styles/PieChart.scss";

export default class PieChart extends Chart {

  create() {
    this.svg = super.createRoot();

    this.color = d3.scaleOrdinal().range(["#16c1c3", "#825ae8"]);
  }

  update(state) {
    if (state) {
      this.drawChart(state)
    }
  }

  drawChart(state) {
    const { width, height } = this.props;

    const div = d3.select("body").append("div").attr("class", "tooltip-chart");

    const pieGroup = this.svg.append("g")
      .attr("transform", `translate(${width / 2}, ${height / 2})`);

    const pie = d3.pie()
      .value(d => d.count)
      .sort(null);

    const radius = Math.min(width, height) / 2;

    this.arc = d3.arc()
      .innerRadius(0)
      .outerRadius(radius);

    const path = pieGroup.selectAll("path")
      .data(pie(state.data));

    path.transition().duration(200).attrTween("d", this.arcTween);

    path
      .enter()
      .append("path")
      .attr("fill", (d, i) => this.color(i))
      .attr("d", this.arc)
      .attr("stroke", "white")
      .attr("stroke-width", "6px")
      .each(function (d) {
        this._current = d;
      })
      .on("mouseover", function (d) {
        div
          .style("left", `${d3.event.pageX}px`)
          .style("top", `${d3.event.pageY}px`)
          .transition()
          .duration(200)
          .style("opacity", 0.9);
        div.html(`${mapIncome[d.data.name]}: ${maskPrice(d.value)}`);
      })
      .on("mouseout", function (d) {
        div.transition().duration(500).style("opacity", 0);
      });
  }

  arcTween = (a) => {
    const i = d3.interpolate(this._current, a);
    this._current = i(1);
    return (t) => this.arc(i(t));
  }

}