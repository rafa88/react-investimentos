import * as d3 from 'd3';
import Chart from '../Chart';

import './Styles/Bar.scss'

export default class BarChart extends Chart {

  create() {
    this.svg = super.createRoot();

    this.color = d3
      .scaleOrdinal()
      .range(["#16c1c3", "#825ae8"]);
  }

  update(state) {
    if (state) {
      this.drawChart(state)
    }
  }

  getTextWidth(text, font) {
    const canvas = this.getTextWidth.canvas || (this.getTextWidth.canvas = document.createElement("canvas"));
    const context = canvas.getContext("2d");
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width > 100 ? metrics.width : 100;
  }

  drawChart(state) {
    const { width, height } = this.props;
    const data = state.data;

    const div = d3.select("body").append("div").attr("class", "tooltip-chart");
    const xAxisNames = data.map(function (d) { return d.xAxis; });
    const rateNames = data[0].values.map(function (d) { return d.name; });
    const biggerText = Math.max(...data.map(v => this.getTextWidth(v.xAxis, '10px')));
    this.areaHeight = height - (biggerText / 1.8);

    this.x0 = d3.scaleBand()
      .range([0, width])
      .domain(d3.range(data.length))
      .rangeRound([0, width])
      .padding(0.5)
      .domain(xAxisNames);

    this.x1 = d3.scaleOrdinal()
      .domain(rateNames)
      .range([0, this.x0.bandwidth()]);

    this.y = d3.scaleLinear()
      .range([this.areaHeight, 0])
      .domain([0, d3.max(data, function (x) { return d3.max(x.values, function (d) { return d.value; }); })]);

    const slice = this.svg
      .append("g")
      .attr("class", "bars")
      .attr('transform', `translate(${(this.x0.bandwidth() / 2) * -1}, 0)`)
      .selectAll(".slice")
      .data(data)
      .enter()
      .append("g")
      .attr("class", "g")
      .attr("transform", (d) => { return `translate(${this.x0(d.xAxis)}, 0)`; });

    slice.selectAll("rect")
      .data(function (d) { return d.values; })
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", (d) => { return this.x1(d.name); })
      .attr("y", (d) => { return this.y(0); })
      .attr("width", this.x0.bandwidth())
      .attr("height", (d) => { return this.areaHeight - this.y(0); })
      .attr('stroke', "white")
      .attr('stroke-width', 2)
      .style("fill", (d) => { return this.color(d.name) })
      .on("mouseover", function (d) {
        div.style("left", `${d3.event.pageX}px`)
          .style("top", `${d3.event.pageY}px`)
          .transition()
          .duration(200)
          .style("opacity", 0.9);
        div.html(`${d.value}`);
      })
      .on("mouseout", function (d) {
        div.transition()
          .duration(500)
          .style("opacity", 0);
      });

    slice.selectAll("rect")
      .transition()
      .delay(function (d) { return Math.random() * 1000; })
      .duration(1000)
      .attr("y", (d) => { return this.y(d.value); })
      .attr("height", (d) => { return this.areaHeight - this.y(d.value); });

    this.drawAxis();
    // this.drawLegend(data, metricPsi);
  }

  drawAxis() {
    const { height, width, margin } = this.props;
    const axis = this.svg
      .append("g")
      .attr("class", "axis")

    axis.append("g")
      .attr("transform", `translate(0, ${this.areaHeight})`)
      .call(d3.axisBottom(this.x0))
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", ".15em")
      .attr("transform", "rotate(-65)");
    axis.append("g")
      .attr("transform", `translate(0, 0)`)
      .call(d3.axisLeft(this.y));

    axis
      .append("g")
      .attr("class", "quantity")
      .append("text")
      .attr("class", "yLeftAxisLabel")
      .attr("fill", "#b6b6b6")
      .attr("text-anchor", "middle")
      .attr("transform", `translate(-40, ${height / 2}) rotate(-90)`)
      .text("quantity");
    axis
      .append("g")
      .attr("class", "intervals")
      .attr("transform", `translate(0,${height + margin.top})`)
      .append("text")
      .attr("class", "xAxisLabel")
      .attr("fill", "#b6b6b6")
      .attr("text-anchor", "middle")
      .attr("transform", `translate(${width / 2}, 15)`)
      .text("meses");
  }

  drawLegend(data, metricPsi) {
    const legend = this.svg.selectAll(".legend")
      .data(data[0].values.map(function (d) { return d.name; }))
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; })
      .style("opacity", "0");

    legend.append("rect")
      .attr("x", 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", (d) => { return this.color(d); });

    legend.append("text")
      .attr("x", 50)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "start")
      .style("color", "#b6b6b6")
      .text(function (d) { return d; });

    legend.transition().duration(500).delay(function (d, i) { return 1300 + 100 * i; }).style("opacity", "1");
  }
}