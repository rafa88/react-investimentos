import React from "react";

import "./Styles/ToastComponent.scss";

class ToastComponent extends React.Component {
  
  constructor(props) {
    super(props);
    this.toast = React.createRef();
  }

  componentDidMount() {
    const element = this.toast.current;
    setTimeout(() => {
      element.className = element.className.replace("toast__show", "");
    }, 5000);
  }

  render() {
    const { message, type } = this.props;
    return (
      <div ref={this.toast} className={`toast toast__show toast__${type}`}>
        <div className="toast__desc">{message}</div>
      </div>
    );
  }
}

export default ToastComponent;
