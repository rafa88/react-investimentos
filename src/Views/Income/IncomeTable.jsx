import React from 'react';
import { connect } from 'react-redux';
import { mapIncome } from '../../Enum';
import { getIncomes, deleteIncome } from "../../Actions/IncomesActions";
import { withRouter } from 'react-router-dom';
import { maskDate, maskPrice } from "../Helpers/Mask";
import PieChartComponent from '../Charts/PieChart/PieChartComponent';

import "./Styles/IncomeTable.scss";

class IncomeTable extends React.Component {
  componentDidMount() {
    this.props.getIncomes();
  }

  formatPie = (incomes) => {
    return Object.entries(
      incomes.reduce(
        (all, curr) => ({
          ...all,
          [curr.type]: all[curr.type]
            ? parseFloat(curr.value) + all[curr.type]
            : parseFloat(curr.value),
        }),
        {}
      )
    ).map((value) => ({ name: value[0], count: value[1] }));
  };

  onDelete = (incomeId) => {
    this.props.deleteIncome(incomeId);
  };

  render() {
    const { incomes, isLoading } = this.props;
    return (
      <div className="income__report">
        {isLoading && <div className="loading">
          <i className="demo-icon icon-spin6 animate-spin"></i>
        </div>}
        <div className="income__report-table">
          <table className="table">
            <thead>
              <tr key="thead">
                <th>Tipo</th>
                <th>Data</th>
                <th>Valor</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {incomes.map((data) => (
                <tr key={data.id}>
                  <td>{mapIncome[data.type]}</td>
                  <td>{maskDate(data.date)}</td>
                  <td>{maskPrice(data.value)}</td>
                  <td>
                    <i
                      className="demo-icon icon-trash-empty"
                      onClick={() => this.onDelete(data.id)}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="income__report-chart">
          <div className="report">
            <div className="report__box report__performance">
              <p>Patrimônio</p>
              {incomes.length > 0 && (
                <PieChartComponent data={this.formatPie(incomes)} />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(
  connect(
    (state) => {
      return {
        incomes: Object.values(state.incomes.incomesByIds || {}),
        isLoading: state.incomes.isLoading,
      };
    },
    (dispatch) => {
      return {
        getIncomes: () => {
          dispatch(getIncomes());
        },
        deleteIncome: (incomeId) => {
          dispatch(deleteIncome(incomeId));
        },
      };
    }
  )(IncomeTable)
);
