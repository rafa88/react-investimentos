export function validateForm(obj = {}) {
  const errors = {};
  if (!obj.password) {
    errors.name = "Please fill in the name";
  }
  if (!obj.email) {
    errors.email = "Please fill in the email";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(obj.email)) {
    errors.email = "Please provide a valid e-mail.";
  }

  return errors;
}
