import React from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";

import { URL_HOME } from "../../RootRoute";
import { signOut } from "../../Actions/AuthActions";
import logo from '../../images/logo.svg';

import './Styles/HeaderComponent.scss';

class HeaderComponent extends React.Component {
  state = {
    menuMobile: false,
  };

  toggleMenu = () => {
    const { menuMobile } = this.state;
    this.setState({menuMobile: !menuMobile })
  }

  signOut = () => {
    this.props.signOut();
  };

  render() {
    const { menuMobile } = this.state;
    return (
      <header className="header">
        <section className="header__center">
          <Link to={URL_HOME}>
            <img src={logo} className="header__image" alt="Gorila" />
          </Link>

          {sessionStorage.getItem("reactInvestimentoToken") &&
            <div className={`menu__mobile ${menuMobile ? "menu__mobile-active" : ""}`} onClick={this.toggleMenu}>
              <span className="menu__mobile-item menu__mobile-item__first"></span>
              <span className="menu__mobile-item menu__mobile-item__second"></span>
              <span className="menu__mobile-item menu__mobile-item__third"></span>
            </div>
          }
          {sessionStorage.getItem("reactInvestimentoToken") && (
            <ul className={`header__menu ${menuMobile ? "header__menu-active": ""}`}>
              <li className="header__menu-item">
                <Link to={URL_HOME}>Investimentos</Link>
              </li>
              <li className="header__menu-item" onClick={this.signOut}>
                Sair
              </li>
            </ul>
          )}
        </section>
      </header>
    );
  }
}

export default withRouter(
  connect(
    null,
    (dispatch) => {
      return {
        signOut: () => {
          dispatch(signOut());
        },
      };
    }
  )(HeaderComponent)
);
