const initialState = {
  isLoading: null,
  email: null,
  emailVerified: null,
  token: null,
  error: null,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_SIGNIN":
    case "FETCH_SIGNUP":
    case "FETCH_AUTH_SIGNIN_TOKEN":
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case "SIGNUP_SUCCESS":
    case "SIGNIN_SUCCESS":
      return {
        ...state,
        isLoading: false,
        email: action.value.user.email,
        emailVerified: action.value.user.emailVerified,
        idToken: action.value.user.idToken,
        error: null,
      };
    case "SIGNUP_ERROR":
    case "SIGNIN_ERROR":
      return {
        ...state,
        isLoading: false,
        error: action.value,
      };
    case "SIGNOUT":
      return {
        ...state,
        initialState,
      };
    default:
      return state;
  }
};
