import { combineReducers } from "redux";

import { incomesReducer } from "./IncomesReducer";
import { authReducer } from "./AuthReducer";

export const Reducers = combineReducers({
  incomes: incomesReducer,
  auth: authReducer,
});
