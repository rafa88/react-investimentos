### Running the local project

* git clone https://rafa88@bitbucket.org/rafa88/react-investimentos.git
* run `react-investimentos` to enter the folder
* run `npm install`
* run `npm start`
* Open [http://localhost:5000](http://localhost:5000) to view it in the browser.
